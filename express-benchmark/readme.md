
Beetl 内核是脚本引擎，Beetl模板会被解析成脚本引擎执行渲染，Beetl脚本引擎本身也可以作为
一些规则，表达式，脚本计算使用，如下是目前近几年还在维护的表达式/脚本引擎的性能测试结果

测试内容有一个简单的求值计算，和一个if语句(spel 不支持if，所以使用了三元表达式)

* Aviator https://gitee.com/mirrors/aviator
* Beetl https://gitee.com/xiandafu/beetl
* Groovy http://www.groovy-lang.org/
* JfireEL https://gitee.com/eric_ds/jfireEL
* Spel https://docs.spring.io/spring-framework/docs/3.2.x/spring-framework-reference/html/expressions.html
* Nashorn https://github.com/openjdk/nashorn
* Jexl3 https://commons.apache.org/proper/commons-jexl/reference/examples.html
* Mvel http://mvel.documentnode.com/#language-guide-for-2.0
* liquor https://gitee.com/noear/liquor
* Magic-script https://gitee.com/ssssssss-team/magic-script
* WastEl https://gitee.com/xiaoch0209/wast



测试项目
* simpleExpress ,一个简单的求值表达式 arg.age+(12+arg.pay)
* ifExpresss 一个if语句.(Spel和JfireEL 不支持，使用了三元表达式代替)
* reflect  能返回表达式的需要外部传入变量，如表达式`arg.age+(12+kk.pay)` 返回`[arg,kk]`
* forExpresss 循环测试。

测试方法
>mvn clean package
>java -jar target/benchmarks.jar

如果脚本语言没有如上测试项目，则是不支持

```
Benchmark               Mode  Cnt          Score          Error  Units
Aviator.forExpresss    thrpt    5     501300.085 ±    42824.080  ops/s
Aviator.ifExpresss     thrpt    5    3787821.461 ±   126020.429  ops/s
Aviator.simpleExpress  thrpt    5    3936340.996 ±    73374.389  ops/s
Beetl.forExpresss      thrpt    5    1338770.299 ±    52497.190  ops/s
Beetl.ifExpresss       thrpt    5    4296662.629 ±   144833.816  ops/s
Beetl.reflect          thrpt    5      91948.475 ±    24820.670  ops/s
Beetl.simpleExpress    thrpt    5    3637048.580 ±   208029.652  ops/s
Groovy.forExpresss     thrpt    5     252549.783 ±     4250.116  ops/s
Groovy.ifExpresss      thrpt    5     274622.265 ±     4591.017  ops/s
Groovy.simpleExpress   thrpt    5     265044.153 ±    11326.922  ops/s
Jexl3.forExpresss      thrpt    5     786773.898 ±    10419.682  ops/s
Jexl3.ifExpresss       thrpt    5    4202306.490 ±    30129.805  ops/s
Jexl3.simpleExpress    thrpt    5    3372108.774 ±   215486.204  ops/s
JfireEL.ifExpresss     thrpt    5   32117578.964 ±   527020.236  ops/s
JfireEL.simpleExpress  thrpt    5   24189543.412 ±   368783.395  ops/s
Liquor.forExpresss     thrpt    5  133415280.514 ±  7403665.267  ops/s
Liquor.ifExpresss      thrpt    5  142671515.534 ±  2569369.295  ops/s
Liquor.simpleExpress   thrpt    5  131626067.420 ±  1391417.428  ops/s
Magic.forExpresss      thrpt    5      78293.797 ±     5757.270  ops/s
Magic.ifExpresss       thrpt    5    8289531.268 ±   108521.742  ops/s
Magic.simpleExpress    thrpt    5    5483078.794 ±   102774.920  ops/s
Mvel.forExpresss       thrpt    5      19791.976 ±      913.688  ops/s
Mvel.ifExpresss        thrpt    5     212241.434 ±     4588.556  ops/s
Mvel.simpleExpress     thrpt    5     289372.025 ±     5343.856  ops/s
Spel.ifExpresss        thrpt    5   17001870.991 ±  1104612.166  ops/s
Spel.simpleExpress     thrpt    5   16854500.373 ±  1020142.253  ops/s
WastEl.forExpresss     thrpt    5  539086848.932 ± 16033762.932  ops/s
WastEl.ifExpresss      thrpt    5  553963092.029 ±  8708085.274  ops/s
WastEl.simpleExpress   thrpt    5  493929991.021 ±  7826211.390  ops/s


```

> score越大越快。如果以上测试方式有问题，请联系我，我不是对所有的脚本引擎熟悉使用

# 测试结果(2024-12-24)

WastEl>>Liquor>>JfireEL=Spel>> Aviator=Beetl=Magic=Jexl3 >>Mvel=Groovy>>Nashorn

* Spel和JfireEL是纯表达式引擎，Beetl,和Aviator，Mvel则是脚本引擎.Groovy，Nashorn是一个新的语言
* 编译成class的性能远大于解释执行的脚本语言
* 解释执行提供了语法的灵活性，更完善的出错提示信息
* reflect 方法是获取表达式需要的外边变量，比如这个对有些业务场景很管用，比如根据表达式可以提示用户需要输入哪些变量，`x+y/2`, 提示用户需要输入x和y ，目前自有Beetl支持





