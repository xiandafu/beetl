package org.beetl.express;

import io.github.wycst.wast.common.expression.Expression;
import io.github.wycst.wast.common.expression.compile.CompilerEnvironment;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Setup;

import javax.script.ScriptException;
import java.util.HashMap;
import java.util.Map;

public class WastEl extends BaseExpressBenchmark{

    Expression addExpression = null;
    Expression ifExpression = null;
    Expression forExpresss = null;
    @Benchmark
    public Object simpleExpress() {
//        Map map = new HashMap();
//        map.put("arg",getArg());
        return addExpression.evaluate(getArg());
    }


    @Benchmark
    @Override
    public Object ifExpresss() {
//        Map map = new HashMap();
//        map.put("arg",getArg());
        return ifExpression.evaluate(getArg());
    }

    @Benchmark
    @Override
    public Object forExpresss() {
        return forExpresss.evaluate(getArg());
    }

    @Setup
    public void init() throws ScriptException {
        // parse mode
//        addExpression = Expression.parse("arg.age+(arg.pay+12)");
//        ifExpression = Expression.parse("arg.age==10?true:false");

        // compile mode
        CompilerEnvironment compilerEnvironment = new CompilerEnvironment();
        compilerEnvironment.setVariableType(Arg.class, "arg");
        // support context as the root
        compilerEnvironment.setSupportedContextRoot(true);
        addExpression = Expression.compile("arg.age+(arg.pay+12)", compilerEnvironment);
        ifExpression = Expression.compile("arg.age==10?true:false", compilerEnvironment);

        // 多行文本暂时需要跳过解析
        compilerEnvironment.setSkipParse(true);
        forExpresss = Expression.compile("int c = 0; for(int i = 0; i < arg.getAge(); ++i){c=c+1;} return c;", compilerEnvironment);
    }

    public static void main(String[] args) throws Exception {
        WastEl wastEl = new WastEl();
        wastEl.init();
        org.openjdk.jmh.Main.main(args);
    }
}
