package org.beetl.express;

import org.noear.liquor.eval.CodeSpec;
import org.noear.liquor.eval.Execable;
import org.noear.liquor.eval.Exprs;
import org.noear.liquor.eval.ParamSpec;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Setup;


/**
 * @author noear 2024/9/20 created
 */
public class Liquor extends BaseExpressBenchmark {
    Execable addCompiledExp2;
    Execable ifCompiledExp;
    Execable forCompiledExp;

    @Benchmark
    public Object simpleExpress() {
        try {
            Object result = addCompiledExp2.exec(getArg());
            return result;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    @Benchmark
    @Override
    public Object ifExpresss() {
        try {
            Object result = ifCompiledExp.exec(getArg());
            return result;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    @Benchmark
    public Object forExpresss() {
        try {
            Object result = forCompiledExp.exec(getArg());
            return result;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Setup
    public void init() {
        //要 java 语法 //有 return 时（前后要有空隔），要自己补 ; 号
        ParamSpec[] parameters = {new ParamSpec("arg", Arg.class)};

        addCompiledExp2 = Exprs.compile(new CodeSpec("arg.getAge() + (arg.getPay() + 12)").parameters(parameters));
        ifCompiledExp = Exprs.compile(new CodeSpec("arg.getAge() == 10 ? true : false").parameters(parameters));
        forCompiledExp = Exprs.compile(new CodeSpec("int c =0; for(int i=0; i<arg.getAge(); i++){c=c+1;} return c;").parameters(parameters));
    }

    public static void main(String[] args) {
        Liquor aviator = new Liquor();
        aviator.init();
        System.out.println(aviator.simpleExpress());
        System.out.println(aviator.ifExpresss());
        System.out.println(aviator.forExpresss());
    }
}