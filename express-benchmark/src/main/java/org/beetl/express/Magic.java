package org.beetl.express;

import org.beetl.core.Script;
import org.beetl.core.script.BeetlScriptEngine;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.ssssssss.script.MagicScript;
import org.ssssssss.script.MagicScriptContext;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.script.SimpleBindings;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 */
public class Magic extends BaseExpressBenchmark {

	MagicScript simpleScript = null;
	MagicScript ifScript = null;
	MagicScript forScript = null;

	String simpleStr = "return arg.age+(arg.pay+12);";
	String ifStr = "if(arg.age==10) {return true ;} else { return  false;}";
	String forStr = "var c =0; for(val in range(0,arg.age)){ c=c+1;}";

	@Benchmark
	public Object simpleExpress() {
		MagicScriptContext context = new MagicScriptContext();
		context.set("arg", getArg());
		Object value = simpleScript.execute(context);
		return value;

	}


	@Benchmark
	@Override
	public Object ifExpresss() {

		MagicScriptContext context = new MagicScriptContext();
		context.set("arg", getArg());
		Object value = ifScript.execute(context);
		return value;
	}

	@Override
	@Benchmark
	public Object forExpresss() {
		MagicScriptContext context = new MagicScriptContext();
		context.set("arg", getArg());
		Object value = forScript.execute(context);
		return value;
	}


	@Setup
	public void init() throws IOException {
		simpleScript = MagicScript.create(simpleStr, null);
		simpleScript.compile();
		ifScript = MagicScript.create(ifStr, null);
		ifScript.compile();
		forScript = MagicScript.create(forStr, null);
		forScript.compile();
	}

	public static void main(String[] args) throws Exception {
//		Magic magic = new Magic();
//		magic.init();
//		System.out.println(magic.simpleExpress());
//		System.out.println(magic.ifExpresss());
//		System.out.println(magic.forExpresss());

				Options opt = new OptionsBuilder()
						.include(Magic.class.getSimpleName()).forks(0)
						.build();
				new Runner(opt).run();
	}
}
