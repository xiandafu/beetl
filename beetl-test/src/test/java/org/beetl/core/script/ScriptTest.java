package org.beetl.core.script;

import java.util.Map;

import org.beetl.core.BasicTestCase;
import org.beetl.core.Script;
import org.beetl.core.resource.StringTemplateResourceLoader;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;

public class ScriptTest extends BasicTestCase
{
	@Test
	public void testFileFunctionRetrun() throws Exception
	{

		Map result = gt.runScript("/script/testscript.html", null);
		AssertJUnit.assertEquals(result.get("a"), 1);
		AssertJUnit.assertEquals(result.get("b"), "b");
		AssertJUnit.assertEquals(result.get("return"), 5);

	}

	@Test
	public void testFileFunction2() throws Exception
	{

		Map result = gt.runScript("/script/testscript2.html", null);
		AssertJUnit.assertEquals(result.get("name"), "lijz");

	}


	@Test
	public void testShared() throws Exception
	{
		StringTemplateResourceLoader resourceLoader = new StringTemplateResourceLoader();
		Script script = gt.getScript("return a+b;",resourceLoader);
		script.binding("a",10);
		script.binding("b",3);
		script.execute();;
		if(script.isSuccess()){
			Object o = script.getResult().get("return");
			AssertJUnit.assertEquals(13,o);
		}else{
			AssertJUnit.fail();
		}


	}


	@Test
	public void testScriptWithOutReturn() throws Exception
	{
		StringTemplateResourceLoader resourceLoader = new StringTemplateResourceLoader();
		Script script = gt.getScript("e+f;",resourceLoader);
		script.binding("e",10);
		script.binding("f",3);
		script.execute();;
		if(script.isSuccess()){
			Object o = script.getResult().get("return");
			AssertJUnit.assertEquals(13,o);
		}else{
			AssertJUnit.fail();
		}


	}

}
