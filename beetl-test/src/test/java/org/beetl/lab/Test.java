package org.beetl.lab;

import jodd.bean.BeanUtil;
import org.beetl.core.*;
import org.beetl.core.config.BeetlConfig;
import org.beetl.core.debug.DebugContext;
import org.beetl.core.debug.DebugLockListener;
import org.beetl.core.debug.MockDebugLock;
import org.beetl.core.exception.BeetlException;
import org.beetl.core.exception.ErrorInfo;
import org.beetl.core.resource.ClasspathResourceLoader;
import org.beetl.core.resource.StringTemplateResourceLoader;
import org.beetl.core.script.BeetlScriptEngine;
import org.beetl.core.statement.Statement;


import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.SimpleBindings;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 用于不写单元测试，快速测试
 * @author xiandafu
 */
class Test {

    /** DEBUG flag */
    private static final boolean DEBUG = BeetlConfig.DEBUG;
    /** Log TAG */
    private static final String TAG = "org.beetl.core.lab.Test";

    public static void main(String[] args) throws Exception {
		debugTest();

    }
	public static void debugTest() throws Exception{
		Configuration conf = Configuration.defaultConfiguration();
		conf.setEngine("org.beetl.core.debug.DebugTemplateEngine");
		GroupTemplate gt = new GroupTemplate(conf);
		StringTemplateResourceLoader loader = new StringTemplateResourceLoader();
		String str ="println(now()); return b ;";
		Script script = gt.getScript(str,loader);
		if(script.validate()!=null){
			BeetlException beetlException =  script.validate();
			System.out.println(new ErrorInfo(beetlException));
			return ;
		}
		Map map = new HashMap();
		map.put("c",500);
		script.binding(map);
		DebugContext debugContext = (DebugContext)script.getContext();
		debugContext.setDebugLock(new MockDebugLock(debugContext));

		debugContext.setDebugLockListener(new DebugLockListener() {
			@Override
			public void notifyHoldon(DebugContext debugContext,int line) {
				//执行到断点
				System.out.println("wait on line "+line+"  vars "+debugContext.currentVars(line));;
			}
		});

		script.execute();
		if(script.isSuccess()){
			System.out.println(script.getResult());
		}else{
			System.out.println(script.getErrorInfo());
		}
	}

	public static void  scriptTest() throws Exception{
		ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
		ScriptEngine beetlEngine = scriptEngineManager.getEngineByName("beetl");
		SimpleBindings simpleBindings = new SimpleBindings();
		simpleBindings.put("b",null);
		Map ret = (Map)beetlEngine.eval("var a=1+b.c; return a;",simpleBindings);
		System.out.println(ret);
	}
	public static void print(String a){
		System.out.println(a);
	}

	public void  set(String a,Context ctx){
		System.out.println(a+ctx.gt);
	}

}
