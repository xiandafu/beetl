package org.beetl.ext.spring;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;


@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Import(BeetlTemplateConfig.class)
@Documented
public @interface EnableBeetl {
}
