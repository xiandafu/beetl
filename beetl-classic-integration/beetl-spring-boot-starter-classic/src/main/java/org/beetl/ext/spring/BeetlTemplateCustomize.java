package org.beetl.ext.spring;

import org.beetl.core.GroupTemplate;

public interface BeetlTemplateCustomize {
	public void customize(GroupTemplate groupTemplate);
}
