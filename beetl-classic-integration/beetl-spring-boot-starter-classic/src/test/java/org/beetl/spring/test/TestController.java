package org.beetl.spring.test;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Controller
public class TestController {
    @GetMapping("/test1")
    public String test1(HttpServletRequest req) {
        req.setAttribute("name","xdf");
        return "hello.btl";
    }
}