package org.beetl.spring.test;

import org.beetl.core.GroupTemplate;
import org.beetl.ext.spring.BeetlTemplateCustomize;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {
	@Bean
	public BeetlTemplateCustomize beetlTemplateCustomize(){
		return new BeetlTemplateCustomize() {
			@Override
			public void customize(GroupTemplate groupTemplate) {
				System.out.println("config gt");
			}
		} ;
	}
}
