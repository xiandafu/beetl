package org.beetl.ext.jfinal;

import com.jfinal.render.Render;
import com.jfinal.render.RenderFactory;
import org.beetl.core.Configuration;
import org.beetl.core.GroupTemplate;
import org.beetl.core.ResourceLoader;
import org.beetl.core.resource.ClasspathResourceLoader;
import org.beetl.core.resource.WebAppResourceLoader;

import java.io.IOException;

public class JFinalBeetlRenderFactory extends RenderFactory {
	public GroupTemplate groupTemplate = null;
	
	public Render getRender(String view) {
		return new JFinalBeetlRender(groupTemplate,view);
	}

	public void config(){

		this.configClassPath("");
	}

	public void configClassPath(String path){

		ClasspathResourceLoader resourceLoader = new ClasspathResourceLoader(path);
		config(resourceLoader);
	}
	
	public void configFilePath(String root){
		
		WebAppResourceLoader resourceLoader = new WebAppResourceLoader(root);
		config(resourceLoader);
	}
	
	public void config(ResourceLoader rs){
		
		if (groupTemplate != null)
		{
			groupTemplate.close();
		}
				
		try
		{

			Configuration cfg = Configuration.defaultConfiguration();
			groupTemplate = new GroupTemplate(rs, cfg);
		}
		catch (IOException e)
		{
			throw new RuntimeException("加载GroupTemplate失败", e);
		}
	}
	
	
}
