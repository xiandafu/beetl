package org.beetl.ext.jfinal;

import com.jfinal.render.Render;
import org.beetl.core.GroupTemplate;
import org.beetl.ext.servlet.WebRender;


public class JFinalBeetlRender extends Render {
	 GroupTemplate groupTemplate;
	public JFinalBeetlRender( GroupTemplate groupTemplate,String view){
		super();
		super.setView(view);
		this.groupTemplate = groupTemplate;
	}
	
	@Override
	public void render() {
		response.setContentType("text/html; charset=" + this.getEncoding());
		WebRender web = new WebRender(groupTemplate);
		web.render(this.view, this.request, this.response, null);

	}

}
