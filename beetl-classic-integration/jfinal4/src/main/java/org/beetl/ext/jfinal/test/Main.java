package org.beetl.ext.jfinal.test;

import org.beetl.ext.jfinal.JFinalBeetlRenderFactory;
import com.jfinal.config.*;
import com.jfinal.server.undertow.UndertowServer;
import com.jfinal.template.Engine;
import org.beetl.core.GroupTemplate;

public class Main extends JFinalConfig {

  public static void main(String[] args) {
    UndertowServer.start(Main.class, 18080, true);
  }

  public void configConstant(Constants me) {
    me.setDevMode(true);
    JFinalBeetlRenderFactory rf = new JFinalBeetlRenderFactory();
    rf.configClassPath("/templates");

    me.setRenderFactory(rf);
    GroupTemplate gt = rf.groupTemplate;
    //其他优化
  }

  public void configRoute(Routes me) {
    me.add("/hello", HelloController.class);
  }

  public void configEngine(Engine me) {}
  public void configPlugin(Plugins me) {}
  public void configInterceptor(Interceptors me) {}
  public void configHandler(Handlers me) {}
}
