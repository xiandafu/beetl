package org.beetl.ext.jfinal;

import com.jfinal.core.Controller;


public class HelloController extends Controller {
  public void index() {
    this.setAttr("name","JFinal World");
    this.render("/hello.html");
  }
}

