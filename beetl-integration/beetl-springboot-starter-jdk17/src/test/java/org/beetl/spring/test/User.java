package org.beetl.spring.test;

import jakarta.persistence.*;

@Entity
@Table(name = "tbl_user")
public class User {
	@Id //这是一个主键
	@GeneratedValue(strategy = GenerationType.IDENTITY)//自增主键
	private Integer id;

	@Column(name = "name",length = 50) //这是和数据表对应的一个列
	private String name;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
