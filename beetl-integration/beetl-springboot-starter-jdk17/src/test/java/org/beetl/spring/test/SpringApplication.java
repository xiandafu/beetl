package org.beetl.spring.test;

import org.beetl.ext.spring6.EnableBeetl;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableBeetl
public class SpringApplication {
    public static void main(String[] args) {
        org.springframework.boot.SpringApplication.run(SpringApplication.class, args);
    }

}
