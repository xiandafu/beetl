package org.beetl.spring.test;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class TestController {
	@Autowired
	UserRepository userRepository;
    @GetMapping("/test1")
    public String test1(HttpServletRequest req) {
    	User user = new User();
    	user.setId(1);
    	user.setName("abcdfsdf");
    	userRepository.save(user);
    	int size = userRepository.findAll().size();
        req.setAttribute("name",user.getName()+"="+size);


        return "hello.btl";
    }
}