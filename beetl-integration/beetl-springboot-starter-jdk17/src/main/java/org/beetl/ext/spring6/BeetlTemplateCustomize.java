package org.beetl.ext.spring6;

import org.beetl.core.GroupTemplate;

public interface BeetlTemplateCustomize {
	public void customize(GroupTemplate groupTemplate);
}
