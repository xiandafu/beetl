package org.beetl.core;

import org.beetl.core.statement.ASTNode;
import org.beetl.core.statement.IVarIndex;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class VarDescription {

	public String varName;

	//变量的属性，收集了，目前暂时未用
	public Set<String> attrList = new HashSet<String>();
	//变量被那些节点使用
	public List<ASTNode> where = new ArrayList<ASTNode>();

	public VarDescription(){

	}


	public String getVarName() {
		return varName;
	}

	public void setVarName(String varName) {
		this.varName = varName;
	}

	public Set<String> getAttrList() {
		return attrList;
	}

	public void setAttrList(Set<String> attrList) {
		this.attrList = attrList;
	}



	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(attrList).append("\n");
		sb.append("where:");
		for (ASTNode w : where) {
			sb.append("索引：").append(((IVarIndex) w).getVarIndex()).append(",").append(w.token.line).append("行");
			sb.append(";");
		}
		sb.append("\n");
		return sb.toString();
	}

}
