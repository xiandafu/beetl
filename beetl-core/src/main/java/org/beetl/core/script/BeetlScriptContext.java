package org.beetl.core.script;

import javax.script.Bindings;
import javax.script.ScriptContext;
import java.io.Reader;
import java.io.Writer;
import java.util.Arrays;
import java.util.List;

public class BeetlScriptContext implements ScriptContext {
    Bindings bindings;
    @Override
    public void setBindings(Bindings bindings, int scope) {
        this.bindings =bindings;
    }

    @Override
    public Bindings getBindings(int scope) {
        return bindings;
    }

    @Override
    public void setAttribute(String name, Object value, int scope) {
        bindings.put(name,value);
    }

    @Override
    public Object getAttribute(String name, int scope) {
        return bindings.get(name);
    }

    @Override
    public Object removeAttribute(String name, int scope) {
        return bindings.remove(name);
    }

    @Override
    public Object getAttribute(String name) {
        return bindings.get(name);
    }

    @Override
    public int getAttributesScope(String name) {
        return 0;
    }

    @Override
    public Writer getWriter() {
        return null;
    }

    @Override
    public Writer getErrorWriter() {
        return null;
    }

    @Override
    public void setWriter(Writer writer) {

    }

    @Override
    public void setErrorWriter(Writer writer) {

    }

    @Override
    public Reader getReader() {
        return null;
    }

    @Override
    public void setReader(Reader reader) {

    }

    @Override
    public List<Integer> getScopes() {
        return Arrays.asList(0);
    }
}
