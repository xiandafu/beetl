package org.beetl.core.script;

import org.beetl.core.exception.ErrorInfo;

import javax.script.ScriptException;

public class BeetlScriptException extends ScriptException {
    ErrorInfo errorInfo =null;
    String script;
    public BeetlScriptException(ErrorInfo errorInfo,Exception cause) {
        super(cause);
        this.errorInfo = errorInfo;


    }
    public String getMessage() {
        return errorInfo.toString();
    }



    public int getLineNumber() {
        return errorInfo.getErrorTokenLine();
    }

}
