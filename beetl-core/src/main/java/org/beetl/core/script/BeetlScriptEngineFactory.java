package org.beetl.core.script;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;
import java.util.Arrays;
import java.util.List;

public class BeetlScriptEngineFactory implements ScriptEngineFactory {
    private static final List<String> names = Arrays.asList("Beetl", "beetl", "js");

    @Override
    public String getEngineName() {
        return "Beetl";
    }

    @Override
    public String getEngineVersion() {
        return "3.x";
    }

    @Override
    public List<String> getExtensions() {
        return Arrays.asList("JS");
    }

    @Override
    public List<String> getMimeTypes() {
        return Arrays.asList("JS");
    }

    @Override
    public List<String> getNames() {
        return names;
    }

    @Override
    public String getLanguageName() {
        return "JS like";
    }

    @Override
    public String getLanguageVersion() {
        return "3";
    }

    @Override
    public Object getParameter(String key) {
        //参考NashornScriptEngineFactory
        switch (key) {
            case "javax.script.name":
                return "javascript";
            case "javax.script.engine":
                return this.getEngineName();
            case "javax.script.engine_version":
                return this.getEngineVersion();
            case "javax.script.language":
                return this.getLanguageName();
            case "javax.script.language_version":
                return this.getLanguageVersion();
            case "THREADING":
                return null;
            default:
                return null;
        }
    }

    @Override
    public String getMethodCallSyntax(String obj, String m, String... args) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getOutputStatement(String toDisplay) {
        return "print(" + toDisplay + ")";
    }

    @Override
    public String getProgram(String... statements) {
         StringBuilder sb = new StringBuilder();
         for(String st:statements){
             sb.append(st).append("\n");
         }
         return sb.toString();
    }

    @Override
    public ScriptEngine getScriptEngine() {
        return new BeetlScriptEngine(this);
    }
}
