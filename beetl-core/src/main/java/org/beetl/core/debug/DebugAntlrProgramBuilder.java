package org.beetl.core.debug;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTree;
import org.beetl.core.AntlrProgramBuilder;
import org.beetl.core.GroupTemplate;
import org.beetl.core.Resource;
import org.beetl.core.engine.GrammarCreator;
import org.beetl.core.statement.BlockStatement;
import org.beetl.core.statement.ProgramMetaData;
import org.beetl.core.statement.Statement;

/**
 * 重新构造AST，增加行断点，并分析变量位置
 */
public class DebugAntlrProgramBuilder extends AntlrProgramBuilder {
	int lastLine = -1;

	public DebugAntlrProgramBuilder(GroupTemplate gt, GrammarCreator gc) {
		super(gt, gc);
		this.pbCtx = new DebugProgramBuilderContext();
		this.data = new DebugProgramMetaData();
	}

	@Override
	public ProgramMetaData build(ParseTree tree, Resource resource) {
		super.build(tree,resource);
		DebugProgramBuilderContext debugBuilderContext = (DebugProgramBuilderContext)pbCtx;
		DebugProgramMetaData debugProgramMetaData = (DebugProgramMetaData)data;
		debugProgramMetaData.setBlockEnvContext((DebugBlockEnvContext)debugBuilderContext.root);
		return debugProgramMetaData;


	}

	protected Statement parseStatment(ParserRuleContext node) {
		Statement statement = super.parseStatment(node);
		if(lastLine<node.getStart().getLine()){
			lastLine = node.getStart().getLine();
			return new WrapDebugHoldonStatement(statement,node.getStart().getLine());
		}
		if(statement instanceof BlockStatement){
			return new WrapDebugHoldonBlockStatement(statement,node.getStart().getLine(),node.getStop().getLine());
		}
		return statement;

	}
}
