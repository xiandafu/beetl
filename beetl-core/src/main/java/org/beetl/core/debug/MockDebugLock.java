package org.beetl.core.debug;

import org.beetl.core.statement.Statement;

/**
 * 模拟debug的暂停，这里不暂停
 */
public class MockDebugLock extends DebugLock{
    public MockDebugLock(DebugContext debugContext) {
        super(debugContext);
    }
    @Override
    protected void doLock(long time,int line){
        if(debugContext.getDebugLockListener()!=null){
            debugContext.getDebugLockListener().notifyHoldon(debugContext,line);
        }
    }
    public void continueRun(){

    }
}
