package org.beetl.core.debug;

import org.beetl.core.VarDescription;

public class DebugVarDescription extends VarDescription {
    //变量申明所在行数
    public int line;
    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line = line;
    }
}
