package org.beetl.core.debug;

import org.beetl.core.Context;
import org.beetl.core.statement.GrammarToken;
import org.beetl.core.statement.Statement;

public class WrapDebugHoldonBlockStatement extends Statement {
    Statement oldStatement = null;
    int startLine;
    int endLine;
    public WrapDebugHoldonBlockStatement(Statement oldStatement,int startLine,int end) {
        super(oldStatement.token);
        this.oldStatement = oldStatement;
        this.startLine =  startLine;
        this.endLine = end;
    }

    @Override
    public void execute(Context ctx) {
        //当运行到下一行的时候，停止
        DebugContext debugContext = (DebugContext) ctx;
        debugContext.getDebugLock().waitRun(debugContext.getLockTime(),this.oldStatement,startLine);
        this.oldStatement.execute(ctx);
        debugContext.getDebugLock().waitRun(debugContext.getLockTime(),this.oldStatement,endLine);
    }
}
