package org.beetl.core.debug;

import org.beetl.core.statement.Statement;

import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class DebugLock {
	private final Lock lock = new ReentrantLock();
	private final Condition condition = lock.newCondition();
	protected DebugContext  debugContext;
	protected int currentLine;
	public DebugLock(DebugContext  debugContext){
		this.debugContext = debugContext;
	}
	public void waitRun(long time, Statement nextStatement,int line){
		if(debugContext.getDebugStatus()==0){
			doLock(time,line);
			return ;
		}else if(debugContext.getDebugStatus()==1){
			Set breaklines = debugContext.getBreakLines();
			if(breaklines.isEmpty()){
				//回退到单行运行
				doLock(time,line);
				return ;
			}
			if(breaklines.contains(line)){
				doLock(time,line);
				return ;
			}
		}else if(debugContext.getDebugStatus()==2){
			//do not lock
		}



    }

	protected void doLock(long time,int line){
		lock.lock();
		try {
			this.currentLine = line;
			if(debugContext.getDebugLockListener()!=null){
				debugContext.getDebugLockListener().notifyHoldon(debugContext,line);
			}
			condition.await(time, TimeUnit.MILLISECONDS);
		}catch (Exception exception){
			throw new IllegalStateException(exception);
		}
		finally {
			lock.unlock();
		}
	}
	public void continueRun(){
		lock.lock();
		try {
			condition.signalAll();
		}catch (Exception exception){
			//ignore
		}
		finally {
			lock.unlock();
		}

	}

	public int getCurrentLine() {
		return currentLine;
	}

	public void setCurrentLine(int currentLine) {
		this.currentLine = currentLine;
	}

	protected void finalize() throws Throwable { }

}
