package org.beetl.core.debug;

import org.beetl.core.AntlrProgramBuilder;
import org.beetl.core.GroupTemplate;
import org.beetl.core.engine.DefaultTemplateEngine;
import org.beetl.core.engine.GrammarCreator;
import org.beetl.core.statement.*;

public class DebugTemplateEngine extends DefaultTemplateEngine {

	@Override
	protected Program getProgram(){
		return  new DebugProgram();
	}
	protected AntlrProgramBuilder getAntlrBuilder(GroupTemplate groupTemplate) {
		GrammarCreator grammarCreator = this.getGrammarCreator(groupTemplate);
		return new DebugAntlrProgramBuilder(groupTemplate, grammarCreator);
	}
}
