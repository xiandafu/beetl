package org.beetl.core.debug;

import org.beetl.core.engine.GrammarCreator;

/**
 * 线上引擎的语法创建者，通过重写 {@code for} 和 {@code while} 语句块来限制循环次数等
 */
class DebugGrammarCreator extends GrammarCreator {

	public DebugGrammarCreator() {

	}


}
