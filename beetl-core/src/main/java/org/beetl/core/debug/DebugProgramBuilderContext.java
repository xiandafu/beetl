package org.beetl.core.debug;

import org.antlr.v4.runtime.ParserRuleContext;
import org.beetl.core.BlockEnvContext;
import org.beetl.core.ProgramBuilderContext;
import org.beetl.core.VarDescription;
import org.beetl.core.statement.ASTNode;
import org.beetl.core.statement.Statement;

import java.util.Objects;

public class DebugProgramBuilderContext extends ProgramBuilderContext {



	public DebugProgramBuilderContext(){

	}

	@Override
	protected void varIndexPoint(BlockEnvContext blockEnvContext, VarDescription vd, int index){
		DebugVarDescription debugVarDescription =(DebugVarDescription) vd;
		//当调试的时候，所在行如果比declareLine大，则应该显示此变量varName，且值是vars[index]
		int declareLine = debugVarDescription.getLine();

		String varName= debugVarDescription.getVarName();
		VarPoint varPoint = new VarPoint(varName,declareLine,index);
        DebugBlockEnvContext  debugBlockEnvContext = (DebugBlockEnvContext)blockEnvContext;
		debugBlockEnvContext.getSet().add(varPoint);

	}
	@Override
	public VarDescription addVar(ASTNode varNode) {
		DebugVarDescription debugVarDescription =(DebugVarDescription) super.addVar(varNode);
		debugVarDescription.setLine(varNode.token.line);
		return debugVarDescription;
	}
	@Override
	public BlockEnvContext newBlockEnvContext(){
		return new DebugBlockEnvContext();
	}
	@Override
	public VarDescription newVarDescription(){
		return new DebugVarDescription();
	}
	@Override
	public void enterBlock(ParserRuleContext ruleContext) {
		super.enterBlock(ruleContext);
		int startLine = ruleContext.getStart().getLine();
		int endLine = ruleContext.getStop().getLine();
		((DebugBlockEnvContext)this.current).setBlockStartLine(startLine);
		((DebugBlockEnvContext)this.current).setBlockEndLine(endLine);

	}
	@Override
	public void exitBlock(Statement statement) {
		super.exitBlock(statement);
	}

	public static class VarPoint{
		String varName;
		int declareLine;
		int index;
		public VarPoint(String varName,int declareLine,int index){
			this.varName = varName ;
			this.declareLine = declareLine;
			this.index = index;
		}
		public String getVarName() {
			return varName;
		}

		public void setVarName(String varName) {
			this.varName = varName;
		}

		public int getDeclareLine() {
			return declareLine;
		}

		public void setDeclareLine(int declareLine) {
			this.declareLine = declareLine;
		}

		public int getIndex() {
			return index;
		}

		public void setIndex(int index) {
			this.index = index;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;
			VarPoint varPoint = (VarPoint) o;
			return declareLine == varPoint.declareLine && Objects.equals(varName, varPoint.varName);
		}

		@Override
		public int hashCode() {
			return Objects.hash(varName, declareLine);
		}
	}
}
