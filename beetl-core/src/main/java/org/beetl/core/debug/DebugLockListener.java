package org.beetl.core.debug;

import org.beetl.core.statement.Statement;

public interface DebugLockListener {
    public void notifyHoldon(DebugContext debugContext,int line);
}
