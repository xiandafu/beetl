package org.beetl.core.debug;

import org.beetl.core.Context;
import org.beetl.core.statement.Statement;

public class WrapDebugHoldonStatement extends Statement {
	Statement oldStatement = null;
	int line;
	public WrapDebugHoldonStatement(Statement oldStatement,int line) {
		super(oldStatement.token);
		this.oldStatement = oldStatement;
		this.line =  line;
	}

	@Override
	public void execute(Context ctx) {
		//当运行到下一行的时候，停止
		DebugContext debugContext = (DebugContext) ctx;
		debugContext.getDebugLock().waitRun(debugContext.getLockTime(),this.oldStatement,line);
		this.oldStatement.execute(ctx);
	}
}
