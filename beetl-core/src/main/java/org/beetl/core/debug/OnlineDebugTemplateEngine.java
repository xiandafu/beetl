package org.beetl.core.debug;

import org.beetl.core.GroupTemplate;
import org.beetl.core.engine.GrammarCreator;
import org.beetl.core.engine.OnlineTemplateEngine;

/**
 * 用于在线debug
 */
public class OnlineDebugTemplateEngine extends DebugTemplateEngine{
    @Override
    protected GrammarCreator getGrammarCreator(GroupTemplate groupTemplate) {
        GrammarCreator result = new OnlineTemplateEngine.OnlineGrammarCreator();
        super.setStrictDisableGrammars(result, groupTemplate);
        return result;
    }
}
