package org.beetl.core.debug;

import org.beetl.core.Context;
import org.beetl.core.GroupTemplate;
import org.beetl.core.cache.ContextBuffer;
import org.beetl.core.statement.Program;

/**
 * 传递一个DebugContext，主要功能是变量名称和变量值
 */
public class DebugProgram extends Program {


	@Override
	public Context newContext(GroupTemplate groupTemplate){
		DebugContext debugContext = new DebugContext(groupTemplate,(DebugProgramMetaData)metaData);
		return debugContext;
	}

	@Override
	public Context newContext(GroupTemplate groupTemplate, ContextBuffer buffer){
		DebugContext debugContext = new DebugContext(groupTemplate,(DebugProgramMetaData)metaData,buffer);
		return debugContext;
	}




}
