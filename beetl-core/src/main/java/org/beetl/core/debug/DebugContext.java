package org.beetl.core.debug;

import org.beetl.core.BlockEnvContext;
import org.beetl.core.Context;
import org.beetl.core.GroupTemplate;
import org.beetl.core.cache.ContextBuffer;

import java.util.*;

/**
 * 用于调试
 */
public class DebugContext extends Context {
	DebugLock debugLock = new DebugLock(this);
	//debug暂停一分钟
	int lockTime = 1000*60;

	DebugProgramMetaData debugProgramMetaData = null;

	private DebugLockListener debugLockListener = null;
	private Set<Integer> breakLines = new TreeSet<>();
	//0 单行调试， 1 运行到断点行 2 忽略所有断点，运行到结束
	private  int debugStatus = 0;

	public Map<String,Object> currentVars(int line){
		Map<String,Object> map = new TreeMap<>();
		DebugBlockEnvContext rootDebugBlockEnvContext = (DebugBlockEnvContext)debugProgramMetaData.getBlockEnvContext();
		List<DebugBlockEnvContext> paths = new ArrayList<>();
		paths.add(rootDebugBlockEnvContext);
		findBlockPaths(rootDebugBlockEnvContext,paths,line);
		for(DebugBlockEnvContext context:paths){
			context.getSet().stream().filter(varPoint -> varPoint.getDeclareLine()<line)
					.forEach(varPoint -> map.put(varPoint.getVarName(),vars[varPoint.getIndex()]));
		}

		//有些全局变量未被模板使用，而不存在map中，需要再补上
		for(Map.Entry<String,Object> entry:globalVar.entrySet()){
			String name = entry.getKey();
			if(!map.containsKey(name)){
				map.put(name,entry.getValue());
			}
		}

		return map;
	}

	/**
	 * 单行调试
	 */
	public int stepOnLine(){
		this.debugStatus = 0;
		debugLock.continueRun();
		return debugLock.getCurrentLine();
	}

	/**
	 * 运行到下一个断点
	 */
	public int stepNextToBreakLine(){
		this.debugStatus = 1;
		debugLock.continueRun();
		return debugLock.getCurrentLine();
	}
	public int stepToFinish(){
		this.debugStatus = 2;
		debugLock.continueRun();
		return debugLock.getCurrentLine();
	}

	/**
	 * 当前停留在哪一行
	 * @return
	 */
	public int currentBreakLine(){
		return debugLock.getCurrentLine();
	}
	public void addBreakLine(int line){
		breakLines.add(line);
	}

	public void removeBreakLine(int line){
		breakLines.remove(line);
	}

	public Set<Integer> getBreakLines() {
		return breakLines;
	}

	public void setBreakLines(Set<Integer> breakLines) {
		this.breakLines = breakLines;
	}

	/**
	 * 根据行找到最近的代码块
	 * @param block
	 * @param line
	 * @return
	 */
	protected void findBlockPaths(DebugBlockEnvContext block, List<DebugBlockEnvContext> paths, int line){
		int start = block.getBlockStartLine();
		int end = block.getBlockEndLine();
		if(start==end){
			//同行block，无法行调试，比如{var a=1;var b=2;},忽略
			paths.remove(block);
			return ;
		}
		if((start<line&&end>=line)||start<=line&&end>line){
			for(BlockEnvContext childBlock:block.getBlockList()){
				DebugBlockEnvContext childDebugBlock = (DebugBlockEnvContext) childBlock;
				paths.add(childDebugBlock);
				findBlockPaths(childDebugBlock,paths,line);
			}

		}



	}





	protected DebugContext(GroupTemplate gt,DebugProgramMetaData debugProgramMetaData) {
		super(gt);
		this.debugProgramMetaData = debugProgramMetaData;

	}

	protected DebugContext(GroupTemplate gt, DebugProgramMetaData debugProgramMetaData, ContextBuffer buffer) {
		super(gt,buffer);

		this.debugProgramMetaData = debugProgramMetaData;
	}

	public DebugLock getDebugLock() {
		return debugLock;
	}

	public void setDebugLock(DebugLock debugLock) {
		this.debugLock = debugLock;
	}

	public DebugProgramMetaData getDebugProgramMetaData() {
		return debugProgramMetaData;
	}

	public void setDebugProgramMetaData(DebugProgramMetaData debugProgramMetaData) {
		this.debugProgramMetaData = debugProgramMetaData;
	}

	public DebugLockListener getDebugLockListener() {
		return debugLockListener;
	}

	public void setDebugLockListener(DebugLockListener debugLockListener) {
		this.debugLockListener = debugLockListener;
	}

	public int getDebugStatus() {
		return debugStatus;
	}

	public void setDebugStatus(int debugStatus) {
		this.debugStatus = debugStatus;
	}

	public int getLockTime() {
		return lockTime;
	}

	public void setLockTime(int lockTime) {
		this.lockTime = lockTime;
	}
}
