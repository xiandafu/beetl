package org.beetl.core.debug;

import org.beetl.core.BlockEnvContext;
import org.beetl.core.Context;
import org.beetl.core.statement.ProgramMetaData;

public class DebugProgramMetaData extends ProgramMetaData {
	//debug运行，会保留变量名字，位置信息
	DebugBlockEnvContext blockEnvContext;
	@Override
	public void initContext(Context ctx) {
		super.initContext(ctx);

	}

	public BlockEnvContext getBlockEnvContext() {
		return blockEnvContext;
	}

	public void setBlockEnvContext(DebugBlockEnvContext blockEnvContext) {
		this.blockEnvContext = blockEnvContext;
	}
}
