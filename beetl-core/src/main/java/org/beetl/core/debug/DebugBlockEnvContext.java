package org.beetl.core.debug;

import org.beetl.core.BlockEnvContext;

import java.util.HashSet;
import java.util.Set;

public class DebugBlockEnvContext extends BlockEnvContext {
	protected int blockStartLine=0;
	//默认结束行数
	protected int blockEndLine = 10*10000;
	protected Set<DebugProgramBuilderContext.VarPoint> set = new HashSet<>();

	public int getBlockStartLine() {
		return blockStartLine;
	}

	public void setBlockStartLine(int blockStartLine) {
		this.blockStartLine = blockStartLine;
	}

	public int getBlockEndLine() {
		return blockEndLine;
	}

	public void setBlockEndLine(int blockEndLine) {
		this.blockEndLine = blockEndLine;
	}

	public Set<DebugProgramBuilderContext.VarPoint> getSet() {
		return set;
	}

	public void setSet(Set<DebugProgramBuilderContext.VarPoint> set) {
		this.set = set;
	}
}
