/*
 [The "BSD license"]
 Copyright (c) 2011-2024  闲大赋 (李家智)
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
 3. The name of the author may not be used to endorse or promote products
     derived from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.beetl.core;

/**
 * 默认的本地调用安全管理器，黑名单方式，不允许调用java.lang.Runtime和Process，以及Class,sun.misc等
 *
 * 在实际项目中，可以考虑白名单方式，只允许调用哪些类，这些类通常是项目封装好的类.
 * 
 * <b>而不要使用此默认类</b>，尤其当你的应用类似用Beetl在线方式，C端用户可以任意输入表达式的时候。黑名单方式不是最安全的方式
 *
 *
 * @author xiandafu
 * @see WhiteListNativeSecurityManager
 */
public class DefaultNativeSecurityManager implements NativeSecurityManager {

	/*
	 * (non-Javadoc)
	 *
	 * @see org.beetl.core.NativeSecurityManager#permit(java.lang.String,
	 * java.lang.Class, java.lang.Object, java.lang.String)
	 */
	@Override
	public boolean permit(Object resourceId, Class c, Object target, String method) {
		if (c.isArray()) {
			// 允许调用，但实际上会在在其后调用中报错。不归此处管理
			return true;
		}
		String name = c.getName();
		String className = null;
		String pkgName = null;
		int i = name.lastIndexOf('.');
		if (i != -1) {
			pkgName = name.substring(0, i);
			className = name.substring(i + 1);

		} else {
			// 无包名，允许调用
			return true;
		}

		if(pkgName.startsWith("java.")){
			if (pkgName.startsWith("java.lang.reflect")) {
				//反射类，不允许调用 https://gitee.com/xiandafu/beetl/issues/I8RU01
				return false;
			}else if(pkgName.startsWith("java.io")||pkgName.startsWith("java.nio")){
//				https://gitee.com/xiandafu/beetl/issues/I9RG2V
				return false;
			}else if (pkgName.startsWith("java.lang")) {
					return !className.equals("Runtime")
							&& !className.equals("Process")
							&& !className.equals("ProcessBuilder")
							&& !className.equals("Thread") // https://gitee.com/xiandafu/beetl/issues/I6RUIP
							&& !className.equals("Class") //https://gitee.com/xiandafu/beetl/issues/I6RUIP#note_17223442
							&& !className.equals("System")
							;
			}else if(pkgName.startsWith("java.beans")){
				//https://gitee.com/xiandafu/beetl/issues/I914H3#note_24939039
				return false;
			}
		}


		if(pkgName.startsWith("org.beetl")){
			//https://gitee.com/xiandafu/beetl/issues/I6RUIP
			return false;
		}

		if(pkgName.startsWith("javax.")){
			return false;
		}

		if(pkgName.startsWith("sun.")){
			return false;
		}

		return true;
	}

}
