/*
 [The "BSD license"]
 Copyright (c) 2011-2024  闲大赋 (李家智)
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
 3. The name of the author may not be used to endorse or promote products
     derived from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.beetl.core;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

/**
 * 本地调用安全管理器，白名单方式
 * @author xiandafu
 * @see DefaultNativeSecurityManager
 */
public class WhiteListNativeSecurityManager implements NativeSecurityManager {
	Pattern callPattern = null;
	public WhiteListNativeSecurityManager(){
		allow(Arrays.asList("java.util"));
	}

	@Override
	public boolean permit(Object resourceId, Class c, Object target, String method) {
		if (c.isArray()) {
			// 允许调用，但实际上会在在其后调用中报错。不归此处管理
			return true;
		}
		String name = c.getName();
		int i = name.lastIndexOf('.');
		if (i == -1) {
			// 无包名，肯定安全，允许调用
			return true;

		}

		return  callPattern.matcher(name).matches();
	}

	/**
	 *  指定白名单，默认是java.util
	 * @param calls ,调用，如 [java.util,java.io.File]
	 */
	public  void allow(List<String> calls){
		StringBuilder sb = new StringBuilder();
		for(String pkg:calls){
			int c = pkg.lastIndexOf('.');
			boolean classCall = false;
			if(Character.isUpperCase(pkg.charAt(c+1))){
				classCall = true;
			}
			if(classCall){
				sb.append(pkg.replace(".","\\."));
			}else{
				sb.append(pkg.replace(".","\\.")).append("\\..*");
			}

			sb.append("|");
		}
		sb.setLength(sb.length()-1);
		callPattern = Pattern.compile(sb.toString());
	}

	public static void main(String[] args) {

		{
			List<String> pkgs = Arrays.asList("java.util","java.io");
			StringBuilder sb = new StringBuilder();
			for(String pkg:pkgs){
				int c = pkg.lastIndexOf('.');
				boolean classCall = false;
				if(Character.isUpperCase(pkg.charAt(c+1))){
					classCall = true;
				}
				if(classCall){
					//pkg+class
					sb.append(pkg.replace(".","\\."));
				}else{
					// pkg
					sb.append(pkg.replace(".","\\.")).append("\\..*");
				}

				sb.append("|");
			}
			sb.setLength(sb.length()-1);

			Pattern pattern = Pattern.compile(sb.toString());
			String call = "java.io.c1";
			System.out.println(pattern.matcher(call).matches());
		}

	}

}
