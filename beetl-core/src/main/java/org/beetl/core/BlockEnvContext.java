package org.beetl.core;

import org.beetl.core.statement.IGoto;
import org.beetl.core.statement.Statement;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class BlockEnvContext {
	public Map<String, VarDescription> vars = new TreeMap<String, VarDescription>();
	// chidren
	public List<BlockEnvContext> blockList = new ArrayList<BlockEnvContext>();
	public BlockEnvContext parent = null;
	 public int gotoValue = IGoto.NORMAL;

	public boolean canStopContinueBreakFlag = false;

	public Statement blockRef = null;

	public Map<String, VarDescription> getVars() {
		return vars;
	}

	public void setVars(Map<String, VarDescription> vars) {
		this.vars = vars;
	}

	public List<BlockEnvContext> getBlockList() {
		return blockList;
	}

	public void setBlockList(List<BlockEnvContext> blockList) {
		this.blockList = blockList;
	}

	public BlockEnvContext getParent() {
		return parent;
	}

	public void setParent(BlockEnvContext parent) {
		this.parent = parent;
		parent.blockList.add(this);
	}

	public void setBlockRef(Statement blockRef) {
		this.blockRef = blockRef;
	}
	public Statement getBlockRef() {
		return blockRef;
	}

	public VarDescription getVarDescrption(String varName) {
		return vars.get(varName);
	}

	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append(vars.toString());

		for (BlockEnvContext block : blockList) {
			sb.append(block).append("\n");
		}

		return sb.append("\n").toString();
	}

}
