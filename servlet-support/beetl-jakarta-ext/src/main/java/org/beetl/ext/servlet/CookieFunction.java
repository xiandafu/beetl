package org.beetl.ext.servlet;


import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import org.beetl.core.Context;
import org.beetl.core.Function;



/**
 * <pre>
 * var allArray = cookie()；
 * var cookie= cookie("userName");
 * </pre>
 *
 * @author xiandafu
 */
public class CookieFunction implements Function {

	public Object call(Object[] paras, Context ctx) {

		HttpServletRequest request = (HttpServletRequest) ctx.getGlobal(WebVariable.REQUEST);
		Cookie[] cookies = request.getCookies();
		if (paras.length == 0) {
			return cookies;
		} else {
			String name = (String) paras[0];
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals(name)) {
					return cookie;
				}
			}
			return null;

		}

	}

}