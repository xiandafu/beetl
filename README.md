---
typora-root-url: ../beetl3.0
---

```text
 ______                 _________  _____     
|_   _ \               |  _   _  ||_   _|    
  | |_) |  .---.  .---.|_/ | | \_|  | |      
  |  __'. / /__\\/ /__\\   | |      | |   _  
 _| |__) || \__.,| \__.,  _| |_    _| |__/ | 
|_______/  '.__.' '.__.' |_____|  |________| 

```

## Beetl3 高速模板引擎

[iBeetl.com](http://ibeetl.com) © 2010 ~ 2024，国内流行模板引擎

[![Maven Central](https://img.shields.io/maven-central/v/com.ibeetl/beetl.svg)](https://mvnrepository.com/search?q=g:com.ibeetl%20AND%20beetl)
<a href="https://gitee.com/xiandafu/beetl/blob/master/LICENSE"><img src="https://img.shields.io/:license-BSD%203-green.svg?style=flat-square"></a>

### 功能 


- 动态页面生成
- 静态页面生成
- 代码生成
- 通过生成XML文本中间格式间接生成PDF，WORD等格式
- 短信，微信等模板内容生成
- 脚本引擎 
- 规则引擎

### 模块

* beetl-core: 核心模块
* beetl-ext:  扩展模块
* beetl-integration:  beetl和各个新框架集成，目前主要是springboot3
* beetl-classic-integration beetl和各个框架集成，如springboot2，jfinal，nutz，struts等
* beetl-test：beetl单元测试
* express-benchmark: beetl 和其他表达式引擎性能测试比较
* template-benchmark: beetl 和其他模板引擎性能测试比较
* grammar:beetl的词法和语法文件
* antlr-support: 支持antlr的各个版本
* beetl-release, 集成beetl-core，beetl-ext，antlr依赖，包含了JDK8，JDK11，JDK17支持
* antlr-support, 支持各个版本的antlr，以及JDK11，JDK17
* beeetl-dependency-all： 同beetl，把所有依赖打包成一个flat jar


### maven


```xml
 <dependency>
    <groupId>com.ibeetl</groupId>
    <artifactId>beetl</artifactId>
    <version>3.19.0.RELEASE</version>
</dependency>
```

### 模板片段

http://ibeetl.com/beetlonline/

#### 1 定义变量

像js那样定义变量和表达式，同时，支持类型说明

```
#: var a = 1,isCheck=false;
#: var<String> list = getString(); //type declare
<h1>hello ${a}</h1>

```
#### 2 循环输出

支持js的各种语法，比如if，for，while，同时for循环支持elsefor

```

#: var array = getData();
#: for(var item in array){
<h1> ${itemLP.index}:${item}</h1>
#:}elsefor {
<h1> empty</h1>
#:}

```

#### 3  使用自己喜欢的定界符，占位符

比如使用`<%`和`%>` 代替  `#:`和`换行` 。beetl支持自定义定界符和占位符，并且，支持自定义俩对定界符和俩对占位符

```html

<% var array = [1,2,3]; 
   for(var k in array){ %>
<h1> #{kLP.index}:#{k}</h1>
<% } %>

```

#### 4  调用方法

```html
${call(1)}
${date(),"yyyy-MM-dd"}
```

#### 5  调用Java方法

可以直接调用Java方法，这些方法能被安全管理

```html
${ @myList.size() }
```

#### 6 支持html 标签

不仅仅是脚本语法，也支持标签语法

```html
<my:tag aa = "1">hello<、my:tag>
  
<my:setTag attr = "1"  var="b,c">${b},${c}<my:setTag>
```



#### 7 include 和 layout

支持各种布局

```javascript
#： var page = "/common.html";
#: include(page,{"title":"用户管理"}){}
```



#### 8 脚本和表达式引擎

可以把beetl当成脚本引擎，它有丰富的功能和优秀的性能

```javascript
var a =1;
return a+b;
```



#### 9 禁止语法

可以禁止任何语法，使得模板跟简单清爽。 比如，不允许模板中出现 赋值语法，或者不允许复杂的表达式，或者不允许java直接调用

```javascript
# var a=1;//not allowd
${a}
```



#### 10 遍历语法树

可以很容易通过定制引擎获取模板结构,比如，能获取到如下模板需要外部输入a，且占位符使用了a和c变量,且调用了date方法

```javascript
#: var c = 1,d = date();
${a},${c}


```



#### 11 定制语法树

Paas应用，要求模板渲染不能超过一定次数，以避免jvm 死机,如下可禁止while循环不超过一定次数

```javascript
#: while(true){
  <h1> hello </h1>
#:}  

```







### 介绍

Beetl  ['biːtl]  3.0，从 [https://github.com/javamonkey/beetl2.0/](https://github.com/javamonkey/beetl2.0/) 迁移过来

QQ交流群：636321496(满),219324263(满),252010126

官网：[ibeetl.com](http://ibeetl.com) 

### 性能 每秒处理量，score越大越好

* 模板性能（参考template-beanchmark)

Beetl>Enjoy>>Rocker>Freemarker>>Thymeleaf==Velociy

```
Benchmark              Mode  Cnt       Score       Error  Units
Beetl.benchmark       thrpt    5  109547.863 ± 17161.576  ops/s
BeetlByte.benchmark   thrpt    5  237799.769 ±  5904.514  ops/s
Enjoy.benchmark       thrpt    5   99695.440 ± 14083.595  ops/s
EnjoyByte.benchmark   thrpt    5  223874.001 ±  7265.307  ops/s
Freemarker.benchmark  thrpt    5   41452.634 ± 15917.119  ops/s
Handlebars.benchmark  thrpt    5   40360.198 ± 24345.048  ops/s
Rocker.benchmark      thrpt    5   63657.017 ±  4653.265  ops/s
Thymeleaf.benchmark   thrpt    5    6457.169 ±   272.613  ops/s
Velocity.benchmark    thrpt    5    8024.042 ±  2097.396  ops/s


```

* 表达式/规则性能 (参考express-benchmark) 


WastEl>>Liquor>>JfireEL=Spel>> Aviator=Beetl=Magic=Jexl3 >>Mvel=Groovy>>Nashorn

> 在线Debug&运行 Beetl脚本 http://ibeetl.com/beetlonline/script.html 

```
Benchmark               Mode  Cnt          Score          Error  Units
Aviator.forExpresss    thrpt    5     501300.085 ±    42824.080  ops/s
Aviator.ifExpresss     thrpt    5    3787821.461 ±   126020.429  ops/s
Aviator.simpleExpress  thrpt    5    3936340.996 ±    73374.389  ops/s
Beetl.forExpresss      thrpt    5    1338770.299 ±    52497.190  ops/s
Beetl.ifExpresss       thrpt    5    4296662.629 ±   144833.816  ops/s
Beetl.reflect          thrpt    5      91948.475 ±    24820.670  ops/s
Beetl.simpleExpress    thrpt    5    3637048.580 ±   208029.652  ops/s
Groovy.forExpresss     thrpt    5     252549.783 ±     4250.116  ops/s
Groovy.ifExpresss      thrpt    5     274622.265 ±     4591.017  ops/s
Groovy.simpleExpress   thrpt    5     265044.153 ±    11326.922  ops/s
Jexl3.forExpresss      thrpt    5     786773.898 ±    10419.682  ops/s
Jexl3.ifExpresss       thrpt    5    4202306.490 ±    30129.805  ops/s
Jexl3.simpleExpress    thrpt    5    3372108.774 ±   215486.204  ops/s
JfireEL.ifExpresss     thrpt    5   32117578.964 ±   527020.236  ops/s
JfireEL.simpleExpress  thrpt    5   24189543.412 ±   368783.395  ops/s
Liquor.forExpresss     thrpt    5  133415280.514 ±  7403665.267  ops/s
Liquor.ifExpresss      thrpt    5  142671515.534 ±  2569369.295  ops/s
Liquor.simpleExpress   thrpt    5  131626067.420 ±  1391417.428  ops/s
Magic.forExpresss      thrpt    5      78293.797 ±     5757.270  ops/s
Magic.ifExpresss       thrpt    5    8289531.268 ±   108521.742  ops/s
Magic.simpleExpress    thrpt    5    5483078.794 ±   102774.920  ops/s
Mvel.forExpresss       thrpt    5      19791.976 ±      913.688  ops/s
Mvel.ifExpresss        thrpt    5     212241.434 ±     4588.556  ops/s
Mvel.simpleExpress     thrpt    5     289372.025 ±     5343.856  ops/s
Spel.ifExpresss        thrpt    5   17001870.991 ±  1104612.166  ops/s
Spel.simpleExpress     thrpt    5   16854500.373 ±  1020142.253  ops/s
WastEl.forExpresss     thrpt    5  539086848.932 ± 16033762.932  ops/s
WastEl.ifExpresss      thrpt    5  553963092.029 ±  8708085.274  ops/s
WastEl.simpleExpress   thrpt    5  493929991.021 ±  7826211.390  ops/s

```


### 基本功能

官网文档: [https://www.kancloud.cn/xiandafu/beetl3_guide](https://www.kancloud.cn/xiandafu/beetl3_guide)

```
Beetl 3 中文文档
第一部分 基础用法
1.1 安装
1.2 快速开始
1.3 模板基础配置
1.4 模板加载器
1.5 定界符与占位符
1.6 注释
1.7 变量定义
1.8 属性
1.9 数学表达式
1.10 循环语句
1.11 条件语句
1.12 异常捕获
1.13 虚拟属性
1.14 函数调用
1.15 安全输出(重要)
1.16 输出格式化
1.17 标签
1.18 调用Java方法与属性
1.19 严格MVC控制
1.20 指令
1.21 错误处理
1.22 Beetl小工具
1.23 Escape
第二部分 高级用法
2.1 配置GroupTemplate
2.2 自定义方法
2.3 自定义格式化函数
2.4 自定义标签
2.5 自定义虚拟属性
2.6 使用额外的资源加载器
2.7 自定义资源加载器
2.8 使用CompositeResourceLoader
2.9 自定义错误处理器
2.10 自定义安全管理器
2.11 注册全局共享变量
2.12 自定义布局
2.13 性能优化
2.14 定制输出
2.15 定制模板引擎
2.16 直接运行Beetl脚本
2.17 模板校验
第三部分 Web 集成
3.1 Web提供的全局变量
3.2 集成技术开发指南
3.3 Servlet集成
3.4 SpringMVC集成
3.5 Spring Boot集成
3.6 Jodd集成
3.7 JFinal4 集成方案
3.8 Nutz集成
3.9 Struts2集成
3.10 整合ajax的局部渲染技术
3.11 在页面输出错误提示信息
```

### 特色功能

* 语法js的子集，非常易学
* 自定义模板占位符和定界符
* 能扩展模板引擎，实现高级功能
* 能修改语法树，实现高级功能
* 字符出和字节输出支持，高性能web表现
* 高性能解释执行，提供特殊语法，如for{} elsefor{},安全输出语法，省略的三元表达式等
* 支持html/xml Tag 语法
* 内核是脚本引擎，除了模板外，也可以用于表达式和规则。

### 支持

任何企业和个人都可以免费使用，并能免费得到社区，论坛，QQ群和作者的免费技术支持。以下情况需要收费技术支持，详情可联系微信（lliijjzz），备注“商业技术支持”

- 任何公开申明了 `996` 工作制度的企业，将收取 `7996 元` / 年 的费用
- 想获得商业技术支持，如培训，技术咨询，定制，售后等，可根据公司规模收取 `1000 - 10000 元` 年费

### 如何贡献

在您贡献代码时，请遵循以下基本代码规范：
- 每行代码不超过120个[等宽字符](https://baike.baidu.com/item/%E7%AD%89%E5%AE%BD%E5%AD%97%E4%BD%93/8434037?fr=aladdin)
- 每个类(Class)、字段(Field)、方法(Method)都必须添加[javadoc注释](https://baike.baidu.com/item/javadoc/4640765?fr=aladdin)(@Override方法可以不加)
- 不能省略if、while后面的大括号
- 不能使用制表符，通过4个空格代替

**注释规范 - 示例**

```java
/*
版权声明部分
*/

package org.beetl.core.cache;

import java.util.function.Function;

/**
 * 缓存的标准接口，每个方法都必须线程安全
 *
 * @author xiandafu
 * @since 2020-09-21
 */
public interface Cache {

    /** Log TAG */
    String TAG = "Cache";

    /**
     * 通过 {@code key} 从缓存中获取对应的 value
     *
     * @param key 键，有可能为 null
     * @return 返回缓存中 {@code key} 所对应的 value，有可能为 null
     */
    Object get(Object key);

    /**
     * 通过 {@code key} 从缓存中获取对应的 value；
     * 如果获取的value为null，则将 {@code function#apply} 方法的返回值作为newValue，添加到缓存中，并返回
     *
     * @param key      键，有可能为 null
     * @param function 函数，在通过key获取value为null时，将执行 {@code function#apply} 方法
     * @return 如果获取的value为null，则将 {@code function#apply} 方法的返回值作为newValue，添加到缓存中，并返回
     */
    Object get(Object key, Function<Object,Object> function);

}
```

**git提交规范 - 示例**

```shell
# git commit 信息应该包含 [发行版本号] + [模块] + [提交信息]
# 例如当前 org.beetl.express.Beetl 是 3.2.0，修改的内容是"语法树结点的注释"，则 commit 信息为：
git commit -m '【Sprint3.3.0】【org.beetl.express.Beetl】语法树结点的注释'
```



Beetl 的成长离不开以下人员的帮助（排名不分先后）：

- [作死模式](javascript:;)
- [一粟蜉蝣](javascript:;)
- [逝水fox](javascript:;)
- [kraken](javascript:;)
- [西安玛雅牛](javascript:;)
- [级?!](javascript:;)
- [orangetys](javascript:;)
- [Oo不懂oO](javascript:;)
- [原上一颗草](javascript:;)
- [龙图腾飞](javascript:;)
- [nutz](javascript:;)
- [天方地圆](javascript:;)
- [九月](javascript:;)
- [Daemons](javascript:;)
- [Gavin.King](javascript:;)
- [Sue](javascript:;)
- [Zhoupan](javascript:;)
- [woate](javascript:;)
- [fitz](javascript:;)
- [darren](http://darren.ink/)
- [zqq](javascript:;)
- [ 醉引花眠](javascript:;)

