# 3.15.11

* 本地函数调用允许附加一个Context

# 3.15.13

* DefaultSecurityManager加强安全
* 代码注释改成2011-2024

# 3.15.14

* 修复字节码生成不支持char bug

# 3.16.0

* 增加Java调用白名单安全管理器WhiteListNativeSecurityManager
* antlr 4.13 支持
* SpringBoot3 +Spring6+JDK21 最新版本支持

# 3.16.2
* DefaultCache调整以略微优化并发性能
* 调整模板引擎的性能测试

# 3.17.0 （不兼容修改）
* 修复 CVE-2024-22533：需要注意，这些Beetl的所谓安全漏洞，是把Beetl的模板编写权给不受控用户，可能会导致。绝大部分情况都不可能发生这个事情

# 3.18.0 
* 集成javax.script
# 3.19.0
* 模板和脚本支持debug
* onlne纳入工程模块
* 新增 http://ibeetl.com/beetlonline/script.html  用来运行和调试脚本
