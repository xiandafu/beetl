# 模板引擎性能测试


## 使用 JMH ，测试结果更权威 

* [Freemarker](http://freemarker.org/)
* [Mustache](https://github.com/spullara/mustache.java)
* [Pebble](http://www.mitchellbosecke.com/pebble)
* [Rocker](https://github.com/fizzed/rocker)
* [Thymeleaf](http://www.thymeleaf.org/)
* [Trimou](http://trimou.org/)
* [Velocity](http://velocity.apache.org/)
* [org.beetl.express.Beetl](http://ibeetl.com/)
* [Enjoy](http://jfinal.com/)

还有些编译成class的模板引擎没有入选，因为他们语法更简单，且模板编译成class，功能有局限

* te4j https://github.com/whilein/te4j
* jte  https://github.com/casid/jte
* httl http://httl.github.io/en/ 长时间不维护



## 运行


1. `mvn clean install`
2. 运行 `java -jar target/benchmarks.jar`
3. 单独运行 `java -jar target/benchmarks.jar Beetl Freemarker



## 目前结果（2024-4-12）越高越好
2线程并发
```

Benchmark              Mode  Cnt       Score       Error  Units
Beetl.benchmark       thrpt    5  109547.863 ± 17161.576  ops/s
BeetlByte.benchmark   thrpt    5  237799.769 ±  5904.514  ops/s
Enjoy.benchmark       thrpt    5   99695.440 ± 14083.595  ops/s
EnjoyByte.benchmark   thrpt    5  223874.001 ±  7265.307  ops/s
Freemarker.benchmark  thrpt    5   41452.634 ± 15917.119  ops/s
Handlebars.benchmark  thrpt    5   40360.198 ± 24345.048  ops/s
Rocker.benchmark      thrpt    5   63657.017 ±  4653.265  ops/s
Thymeleaf.benchmark   thrpt    5    6457.169 ±   272.613  ops/s
Velocity.benchmark    thrpt    5    8024.042 ±  2097.396  ops/s


```

> 注意，最新测试包含了二进制直接输出，BeetlByte.benchmark，EnjoyByte.benchmark支持且性能非常好

### 总结

* 对比其他流行模板引擎Beetl性能非常优秀， https://www.kancloud.cn/xiandafu/javamicrotuning。
* Thymeleaf 和 Velocity 性能是最糟糕的，这个测试毫无疑问又一次证明
* 并发测试考虑到机器性能原因，有可能不准，供参考。可以查看单线程性能测试结果，这能体现模板引擎的能力


