package com.mitchellbosecke.benchmark;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.Properties;

import com.jfinal.template.io.WriterBuffer;
import com.mitchellbosecke.benchmark.io.CharStream;
import com.mitchellbosecke.benchmark.io.NoneStream;
import org.beetl.core.Configuration;
import org.beetl.core.GroupTemplate;
import org.beetl.core.Template;
import org.beetl.core.io.CachedStringWriter;
import org.beetl.core.io.NoLockStringWriter;
import org.beetl.core.io.SoftReferenceWriter;
import org.beetl.core.resource.ClasspathResourceLoader;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

public class Beetl extends BaseBenchmark {

	private Map<String, Object> context;
	GroupTemplate gt = null;

	@Setup
	public void setup() throws IOException {
		ClasspathResourceLoader resourceLoader = new ClasspathResourceLoader("/");
		Properties ps = new Properties();
		Configuration cfg = new Configuration(ps);
//		cfg.setDirectByteOutput(true);
		cfg.setStatementStart("@");
		cfg.setStatementEnd(null);
		context = this.getContext();
		gt = new GroupTemplate(resourceLoader, cfg);


	}



	@Benchmark
	@Override
	public void benchmark(Blackhole blackhole) throws IOException {
		Template template = gt.getTemplate("/templates/stocks.beetl.html");
		template.binding(context);
		template.renderTo(new CharStream("UTF-8"));
		blackhole.consume(1);

	}
	public static void main(String[] args) throws RunnerException {

		Options opt = new
				OptionsBuilder()
				.include(Beetl.class.getSimpleName()).forks(0)
				.build();
		new Runner(opt).run();
	}

}
