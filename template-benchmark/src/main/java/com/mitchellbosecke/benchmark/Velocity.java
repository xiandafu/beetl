package com.mitchellbosecke.benchmark;

import java.io.StringWriter;
import java.io.Writer;
import java.util.Properties;

import com.mitchellbosecke.benchmark.io.CharStream;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.OptionsBuilder;

public class Velocity extends BaseBenchmark {

	VelocityEngine engine = null;

	@Setup
	public void setup() {
		Properties configuration = new Properties();
		configuration.setProperty("resource.loader", "class");
		configuration.setProperty("class.resource.loader.class",
				"org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");

		engine = new VelocityEngine(configuration);
	}

	@Benchmark
	public void benchmark(Blackhole blackhole) {

		VelocityContext context = new VelocityContext(getContext());
		Template template = engine.getTemplate("templates/stocks.velocity.html", "UTF-8");
		CharStream charStream =  new CharStream("UTF-8");
		template.merge(context, charStream);

	}

	public static void main(String[] args) throws RunnerException {

		org.openjdk.jmh.runner.options.Options opt = new
				OptionsBuilder()
				.include(Velocity.class.getSimpleName()).forks(0)
				.build();
		new Runner(opt).run();
	}


}
