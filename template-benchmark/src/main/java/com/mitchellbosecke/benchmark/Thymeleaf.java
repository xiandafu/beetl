package com.mitchellbosecke.benchmark;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Locale;

import com.mitchellbosecke.benchmark.io.CharStream;
import freemarker.template.Template;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.context.IContext;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

import freemarker.template.TemplateException;

public class Thymeleaf extends BaseBenchmark {

    private TemplateEngine engine;

    private IContext context;

    @Setup
    public void setup(Blackhole blackhole) throws IOException {
        engine = new TemplateEngine();
        engine.setTemplateResolver(new ClassLoaderTemplateResolver());
        context = new Context(Locale.getDefault(), getContext());
    }

    @Benchmark
    public void benchmark(Blackhole blackhole) throws TemplateException, IOException {
        CharStream charStream =  new CharStream("UTF-8");
		engine.process("templates/stocks.thymeleaf.html", context, charStream);

    }

    public static void main(String[] args) throws RunnerException {

        org.openjdk.jmh.runner.options.Options opt = new
                OptionsBuilder()
                .include(Thymeleaf.class.getSimpleName()).forks(0)
                .build();
        new Runner(opt).run();
    }



}