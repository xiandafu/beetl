package com.mitchellbosecke.benchmark;

import com.mitchellbosecke.benchmark.io.CharStream;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.rythmengine.RythmEngine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

public class Rythm extends BaseBenchmark {

    RythmEngine engine;

    String template;

    @Setup
    public void setup(Blackhole blackhole) throws IOException {
        Properties properties = new Properties();
        properties.put("log.enabled", false);
        properties.put("feature.smart_escape.enabled", false);
        properties.put("feature.transform.enabled", false);
        engine = new RythmEngine(properties);
        template = readResource("templates/stocks.rythm.html");
    }
//    @Benchmark
    public void benchmark(Blackhole blackhole) {
         engine.render(new CharStream("UTF-8"),template, getContext());
    }
    public static String readResource(String name) throws IOException {
        StringBuilder builder = new StringBuilder();
        try (BufferedReader in = new BufferedReader(
                new InputStreamReader(Rythm.class.getClassLoader().getResourceAsStream(name))
        )) {
            for (;;) {
                int c = in.read();
                if (c == -1) {
                    break;
                }
                builder.append((char) c);
            }
        }
        return builder.toString();
    }

    public static void main(String[] args) throws RunnerException {

        org.openjdk.jmh.runner.options.Options opt = new
                OptionsBuilder()
                .include(Rythm.class.getSimpleName()).forks(0)
                .build();
        new Runner(opt).run();
    }
}
