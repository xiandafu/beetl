package com.mitchellbosecke.benchmark;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;

import com.mitchellbosecke.benchmark.io.CharStream;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Setup;

import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

public class Freemarker extends BaseBenchmark {

Configuration configuration ;

    @Setup
    public void setup() throws IOException {
         configuration = new Configuration(Configuration.VERSION_2_3_22);
        configuration.setTemplateLoader(new ClassTemplateLoader(getClass(), "/"));
       
    }

    @Benchmark
    public void benchmark(Blackhole blackhole) throws TemplateException, IOException {
		CharStream charStream = new CharStream("UTF-8");
		Template template = configuration.getTemplate("templates/stocks.freemarker.html");
		template.process(getContext(), charStream);

    }

    public static void main(String[] args) throws RunnerException {

        Options opt = new
                OptionsBuilder()
                .include(Freemarker.class.getSimpleName()).forks(0)
                .build();
        new Runner(opt).run();
    }



}
