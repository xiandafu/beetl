package com.mitchellbosecke.benchmark;

import com.mitchellbosecke.benchmark.io.NoneStream;
import org.beetl.core.Configuration;
import org.beetl.core.GroupTemplate;
import org.beetl.core.Template;
import org.beetl.core.resource.ClasspathResourceLoader;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.io.IOException;
import java.util.Map;
import java.util.Properties;

public class BeetlByte extends BaseBenchmark {

	private Map<String, Object> context;
	GroupTemplate gt = null;

	@Setup
	public void setup() throws IOException {
		ClasspathResourceLoader resourceLoader = new ClasspathResourceLoader("/");
		Properties ps = new Properties();
		Configuration cfg = new Configuration(ps);
		cfg.setDirectByteOutput(true);
		cfg.setStatementStart("@");
		cfg.setStatementEnd(null);
		context = this.getContext();
		gt = new GroupTemplate(resourceLoader, cfg);


	}

	public static void main(String[] args) throws RunnerException {

		Options opt = new
				OptionsBuilder()
				.include(BeetlByte.class.getSimpleName()).forks(0)
				.build();
		new Runner(opt).run();
	}

	@Benchmark
	@Override
	public void benchmark(Blackhole blackhole) throws IOException {
		Template template = gt.getTemplate("/templates/stocks.beetl.html");
		template.binding(context);
		template.renderTo(new NoneStream());
		return ;
	}


}
