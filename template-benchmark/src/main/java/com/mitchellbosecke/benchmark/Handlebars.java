
package com.mitchellbosecke.benchmark;
import java.io.IOException;
import java.io.StringWriter;

import com.mitchellbosecke.benchmark.io.CharStream;
import com.mitchellbosecke.benchmark.io.NoneStream;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Setup;

import com.github.jknack.handlebars.Handlebars.SafeString;
import com.github.jknack.handlebars.Helper;
import com.github.jknack.handlebars.Options;
import com.github.jknack.handlebars.Template;
import com.github.jknack.handlebars.io.ClassPathTemplateLoader;
import com.mitchellbosecke.benchmark.model.Stock;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.OptionsBuilder;

public class Handlebars extends BaseBenchmark {

    private Object context;

    private Template template;

    @Setup
    public void setup() throws IOException {
        template = new com.github.jknack.handlebars.Handlebars(new ClassPathTemplateLoader("/", ".html"))
                .registerHelper("minus", new Helper<Stock>() {
                    @Override
                    public CharSequence apply(final Stock stock, final Options options)
                            throws IOException {
                        return stock.getChange() < 0 ? new SafeString("class=\"minus\"") : null;
                    }
                }).compile("templates/stocks.hbs");
        this.context = getContext();
    }

    @Benchmark
    public void benchmark(Blackhole blackhole) throws IOException {
		template.apply(context,new CharStream("UTF-8"));
    }
    public static void main(String[] args) throws RunnerException {

        org.openjdk.jmh.runner.options.Options opt = new
                OptionsBuilder()
                .include(Handlebars.class.getSimpleName()).forks(0)
                .build();
        new Runner(opt).run();
    }

}